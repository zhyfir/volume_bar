#!/bin/bash

get_volume() {
	state=`amixer sget Master | grep 'Right:' | awk -F'[][]' '{ print $4 }'`
	
	if [ $state = "on" ]; then
		volume=`amixer sget Master | grep 'Right:' | awk -F'[]%[]' '{ print $2 }'`
	else
		volume=0
	fi
	
	echo $volume
}

draw() {
	id=1
	
	vol=$(get_volume)
	bar_length=20
	actual_vol=$(($vol / 5))
	vol_bar=""
	vol_char="█"
	vol_empty="░"
	
	i=0
	
	while [ $i -lt $actual_vol ]
	do
		vol_bar+="${vol_char}"
		((i++))
	done
	
	while [ $i -lt $bar_length ]
	do
		vol_bar+="${vol_empty}"
		((i++))
	done
	
	notify-send -r $id $vol_bar
}

get_action() {
	step=3%
	
	case $1 in
		"up")
			amixer set Master $step+;;
		"down")
			amixer set Master $step-;;
		"toggle")
			amixer set Master toggle;;
		*)
			echo "Invalid option";;
	esac
	
	draw
}

main() {
	if [[ $# -eq 1 ]]; then
		get_action $1
	fi
}

main $1
